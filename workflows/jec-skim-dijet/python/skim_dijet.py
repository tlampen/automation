#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandler,dbs_data_source
from ecalautoctrl.TaskHandlers import LockBase
from typing import Union, List, Optional, Dict
from datetime import datetime

def process_by_intlumi(target: float=0., nogaps: bool=True):
    """
    This decorator adds the a :func:`groups` function that implements run splitting based on the amount of collected data.

    It should be used to decorate a class that inherits from :class:`~ecalautoctrl.HandlerBase`.

    :param target: target recorded integrated luminosity (in /pb).
    :param nogaps: do not allow gaps between runs in a given group. This implies that only subsequent runs for which the
    task is injected will be used to build a group. If a dependency is not fulfilled for a run the group is not processed.
    Therefore the integrated luminosity target is computed on all runs injected in the automation,
    regardless of the dependency status. This ensures building reproducible groups (default behaviour).
    If the option is False groups are built to reach the desired lumi figure regardless of continuity.
    """
    def wrapper(cls):
        def groups(self) -> List[List[Dict]]:
            """
            Group runs to reach a desired integrated lumi.
            This function is added by :deco:`~ecalautoctrl.process_by_intlumi`.
            """
            # Groups are built from all runs injected for this task in case of no gaps. Dependencies in this case
            # are checked once the groups have been built.
            # Otherwise if gaps are allowed the dependencies are enforced at this stage.
            wdeps = {self.task: 'new'}
            if not nogaps:
                wdeps.update(self.wdeps)
            runs=self.rctrl.getRuns(status=wdeps)
            wdeps = {self.task: 'reprocess'}
            if not nogaps:
                wdeps.update(self.wdeps)
            runs.extend(self.rctrl.getRuns(status=wdeps))
            # sort by run number to create groups regardless if the tasks are in new or reprocess status
            runs = sorted(runs, key=lambda r: r['run_number'])

            # apply lock on the fetched runs
            lock = self.lock(runs=runs)
            runs = [run for i, run in enumerate(runs) if not lock[i]]

            grps = [[]]
            clumi = 0.
            for run in runs:
                clumi += run['lumi']
                grps[-1].append(run)
                # enough lumi, new group
                if clumi >= target:
                    grps.append([])
                    clumi = 0.

            # remove the last group which is the growing one
            if clumi < target:
                grps.pop()

            # check dependencies in case nogaps is set.
            if nogaps:
                self.wdeps.update({self.task: 'new'})
                available_runs = self.rctrl.getRuns(status=self.wdeps)
                self.wdeps.update({self.task: 'reprocess'})
                available_runs.extend(self.rctrl.getRuns(status=self.wdeps))
                available_runs = [r['run_number'] for r in available_runs]

                # remove groups that do not fulfill the dependencies
                grps = [grp for grp in grps if all(r['run_number'] in available_runs for r in grp)]

            return grps

        setattr(cls, 'groups', groups)
        return cls
    return wrapper


@dbs_data_source
@process_by_intlumi(target=1000.0) # 1 inverse femtobarn
class HTCHandlerByIntLumi(HTCHandler):
    """
    HTCHandler that processes by integrated luminosity. No previous task dependencies.
    """
    
    def __init__(self,
                task: str,
                dsetname=Union[str, List[str]],
                deps_tasks: Optional[Union[str, List[str]]] = None,
                **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.dsetname = dsetname

class TimeLock(LockBase):
    def __init__(self,
                timelimit = 60, # hours
                **kwargs):
        super().__init__(**kwargs)
        self.timelimit = timelimit

        self.log.info(f"TimeLock - Waiting {self.timelimit} hours until processing the run")

    def __str__(self):
        return 'TimeLock'

    def lock(self, runs : List[Dict]) -> List[bool]:
        lock_list = len(runs)*[False]
        time_now = datetime.utcnow()
        date_format = "%Y-%m-%dT%H:%M:%SZ" # This is from RunCtrl

        for i, run in enumerate(runs):
            
            date_string = run["endtime"]

            time_then = datetime.strptime(date_string, date_format)

            time_diff = (time_now - time_then).total_seconds()

            if time_diff < self.timelimit * 60 * 60:
                lock_list[i] = True

        return lock_list

if __name__ == '__main__':

    # Locks to wait for PromptReco
    timelock = TimeLock()

    handler = HTCHandlerByIntLumi(task='jec-skim-dijet', dsetname='/JetMET*/*PromptReco*/NANOAOD',locks=[timelock])

    ret = handler()

    sys.exit(ret)
