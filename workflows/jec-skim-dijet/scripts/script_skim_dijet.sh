#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

NTHREADS=`nproc`
echo "Number of threads: $NTHREADS"

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/j/jecpcl

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here
mkdir out_skim

RUN_RANGE=`python3 $WDIR/src/dijet_rdf/src/main.py find_range --filelist $INFILE --nThreads $NTHREADS --is_local`

JSON_FILES=`ls -m /eos/user/c/cmsdqm/www/CAF/certification/Collisions24/DCSOnly_JSONS/dailyDCSOnlyJSON/Collisions24_13p6TeV*`
GOLDEN_JSON=`python3 $WDIR/src/dijet_rdf/src/main.py find_json --run_range $RUN_RANGE --json_file "${JSON_FILES}"`

python3 $WDIR/src/dijet_rdf/src/main.py skim \
	--filelist $INFILE \
	--golden_json $GOLDEN_JSON \
	--triggerpath $WDIR/src/dijet_rdf/data/triggerlists/JME_triggers_skim.txt \
	--out out_skim \
	--run_range $RUN_RANGE \
	--channel dijet \
	--nThreads $NTHREADS \
	--is_local
RETSKIM=$?

mkdir out_hist

HISTFILE=`ls out_skim/*root`
HISTFILE=$(echo $HISTFILE | tr " " ",")

python3 $WDIR/src/dijet_rdf/src/main.py hist \
	--filelist $HISTFILE \
	--hist_config $WDIR/src/dijet_rdf/data/histograms/JME_histograms.ini \
	--triggerpath $WDIR/src/dijet_rdf/data/triggerlists/JME_triggers.txt \
	--out out_hist \
	--run_range $RUN_RANGE \
	--run_tag dijet \
	--nThreads $NTHREADS \
	--is_local

RETHIST=$?

RETCMSSW=$(echo "$RETSKIM+$RETHIST" | bc)

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR/dijet/out_skim
    cp out_skim/*root $EOSDIR/dijet/out_skim
    OFILE=`ls -m $EOSDIR/dijet/out_skim/*root`

    mkdir -p $EOSDIR/dijet/out_hist
    cp out_hist/*root $EOSDIR/dijet/out_hist
    RETCOPY=$?
    
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
