#!/bin/bash

set -x

CLUSTERID=${1}
JOBID=${2}
INFILE=${3}
TASK=${4}
EOSDIR=${5}
WDIR=${6}
GT=${7}

NTHREADS=`nproc`
echo "Number of threads: $NTHREADS"

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/j/jecpcl

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

ecalautomation.py $TASK jobctrl --id $JOBID --running --fields "htc-id:${CLUSTERID}"

# Execute the actual processing here
mkdir out_ratio
RETCMSSW=$?

MC_FILES=`ls -m /eos/user/j/jecpcl/public/2024_physics_v2/jec_mc/mc/multijet/out_skim/*`
python3 $WDIR/src/dijet_rdf/src/main.py produce_ratio \
	--data_files $INFILE \
	--mc_files "${MC_FILES}" \
	--data_tag multijet \
	--mc_tag mc_multijet \
	--hist_config $WDIR/src/dijet_rdf/data/histograms/JME_histograms.ini \
	--out out_ratio \
	--nThreads $NTHREADS
RETCMSSW=$(echo "$RETCMSSW+$?" | bc)

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR/multijet/out_ratio
    cp out_ratio/*root $EOSDIR/multijet/out_ratio
    OFILE=`ls out_ratio/*root`
    OFILE=$EOSDIR/multijet/out_ratio/`basename "${OFILE}"`
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

if [ "$RET" == "0" ]
then
    ecalautomation.py $TASK jobctrl --id $JOBID --done --fields "output:${OFILE}"
else
    ecalautomation.py $TASK jobctrl --id $JOBID --failed
fi

exit $RET
