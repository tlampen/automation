#!/bin/bash

set -x

WDIR=${1}
EOSDIR=${2}

NTHREADS=`nproc`
echo "Number of threads: $NTHREADS"

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

echo $WDIR
echo $EOSDIR

export HOME=/afs/cern.ch/user/j/jecpcl

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

# Execute the actual processing here
INFILES=`ls -mv /eos/user/j/jecpcl/public/2024_physics_v2/jec_skim/*/photonjet/out_skim/*`

mkdir out
RETCMSSW=$?

python3 $WDIR/src/dijet_rdf/src/main.py produce_time_evolution \
	--filelist "${INFILES}" \
	--triggerpath $WDIR/src/dijet_rdf/data/triggerlists/EGM_triggers.txt \
	--data_tag egamma \
	--hist_config $WDIR/src/dijet_rdf/data/histograms/EGM_histograms.ini \
	--out out \
	--nThreads $NTHREADS
RETCMSSW=$(echo "$RETCMSSW+$?" | bc)

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR/photonjet
    cp out/*root $EOSDIR/photonjet
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

exit $RET
