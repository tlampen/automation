#!/bin/bash

set -x

WDIR=${1}
EOSDIR=${2}

NTHREADS=`nproc`
echo "Number of threads: $NTHREADS"

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

echo $WDIR
echo $EOSDIR

export HOME=/afs/cern.ch/user/j/jecpcl

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

# Execute the actual processing here
INFILES_DIJET=`ls -mv /eos/user/j/jecpcl/public/2024_physics_v2/jec_skim/*/dijet/out_skim/*`
INFILES_MULTIJET=`ls -mv /eos/user/j/jecpcl/public/2024_physics_v2/jec_skim/*/multijet/out_skim/*`

mkdir out
RETCMSSW=$?

python3 $WDIR/src/dijet_rdf/src/main.py produce_time_evolution \
	--filelist "${INFILES_DIJET}" \
	--triggerpath $WDIR/src/dijet_rdf/data/triggerlists/JME_triggers.txt \
	--data_tag dijet \
	--hist_config $WDIR/src/dijet_rdf/data/histograms/JME_histograms.ini \
	--out out \
	--nThreads $NTHREADS
RETCMSSW=$(echo "$RETCMSSW+$?" | bc)

python3 $WDIR/src/dijet_rdf/src/main.py produce_time_evolution --filelist "${INFILES_MULTIJET}" --triggerpath $WDIR/src/dijet_rdf/data/triggerlists/JME_triggers.txt --data_tag multijet --hist_config $WDIR/src/dijet_rdf/data/histograms/JME_histograms.ini --out out --nThreads $NTHREADS
RETCMSSW=$(echo "$RETCMSSW+$?" | bc)

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR/jetmet
    cp out/*root $EOSDIR/jetmet
    RETCOPY=$?
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

exit $RET
