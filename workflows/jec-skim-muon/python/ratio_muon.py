#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandlerByRunDBS,process_by_intlumi,HTCHandler,dbs_data_source,process_by_run,prev_task_data_source
from typing import Union, List, Optional

@prev_task_data_source
@process_by_intlumi(target=1000.0)
class HTCHandlerByIntLumi(HTCHandler):
    """
    HTCHandler that processes by integrated luminosity.
    """
    
    def __init__(self,
                task: str,
                prev_input: str,
                deps_tasks: Optional[Union[str, List[str]]] = None,
                **kwargs):
        if deps_tasks is None:
            deps_tasks = [prev_input]
        else:
            deps_tasks.append(prev_input)
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

if __name__ == '__main__':

    handler = HTCHandlerByIntLumi(task='jec-ratio-muon', prev_input='jec-skim-muon')

    ret = handler()

    sys.exit(ret)
