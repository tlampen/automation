#!/bin/bash

set -x

DATA=${1}
WDIR=${2}
EOSDIR=${3}
STEP=${4}
NSTEPS=${5}

echo $DATA
echo $WDIR
echo $EOSDIR
echo $STEP
echo $NSTEPS

NTHREADS=`nproc`
echo "Number of threads: $NTHREADS"

trap 'echo "Kill signal received"; ecalautomation.py $TASK jobctrl --id $JOBID --failed; exit' SIGKILL SIGTERM

export HOME=/afs/cern.ch/user/j/jecpcl

source /cvmfs/cms.cern.ch/cmsset_default.sh

cd $WDIR
eval $(scram runtime -sh)
cd -

# Execute the actual processing here
mkdir out_skim

RETCMSSW=0

python3 $WDIR/src/dijet_rdf/src/main.py skim \
	--filepaths $WDIR/src/dijet_rdf/data/MC_Summer24/$DATA.txt \
	--triggerpath $WDIR/src/dijet_rdf/data/triggerlists/EGM_triggers_skim.txt \
	--out out_skim \
	--mc_tag $DATA \
	--channel photonjet \
	--nThreads $NTHREADS \
	--is_mc \
	--nsteps $NSTEPS \
	--step $STEP
RETCMSSW=$(echo "$RETCMSSW+$?" | bc)

mkdir out_hist

HISTFILE=`ls out_skim/*photonjet*root`
HISTFILE=$(echo $HISTFILE | tr " " ",")
python3 $WDIR/src/dijet_rdf/src/main.py hist \
	--filelist $HISTFILE \
	--hist_config $WDIR/src/dijet_rdf/data/histograms/EGM_histograms.ini \
	--triggerpath $WDIR/src/dijet_rdf/data/triggerlists/EGM_triggers.txt \
	--out out_hist \
	--run_tag "${DATA}_photonjet_${STEP}" \
	--nThreads $NTHREADS \
	--is_local
RETCMSSW=$(echo "$RETCMSSW+$?" | bc)

if [ "$RETCMSSW" == "0" ]
then
    # move the file to the final location (example)
    mkdir -p $EOSDIR/mc/photonjet/out_skim
    cp out_skim/*root $EOSDIR/mc/photonjet/out_skim
    OFILE=`ls out_skim/*root`
    OFILE=$EOSDIR/mc/photonjet/out_skim/`basename $OFILE`

    mkdir -p $EOSDIR/mc/photonjet/out_hist
    cp out_hist/*root $EOSDIR/mc/photonjet/out_hist
    RETCOPY=$?
    
else
    RETCOPY=1
fi

RET=$(echo "$RETCMSSW+$RETCOPY" | bc)

exit $RET
