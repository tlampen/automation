#!/usr/bin/env groovy
package jecctrl.vars

class GlobalVars{
    static def setenv(script, jobname)
    {   
        // Edit this with your subsystem variables
        switch(jobname) {
            case ~/.*-prod$/:
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/tlampen/automation:prod"                
		        script.env.image_setup = "source /home/jecpcl/setup.sh; module load lxbatch/tzero" // Your image setup script
                script.env.dbinstance = "jec_test_v1" // The InfluxDB where you write - usually different between prod and repro
                script.env.campaign = "prod" // The campaign tag, can also be a list of campaigns with a space in between
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/5qp5it4w1tnm8je6xt8ya3jcze" // The mattermost hook for notification - optional functionality
                script.env.eospath = "/eos/user/j/jecpcl/public/test" // Where you want to save stuff etc...
                script.env.eosplots = "/eos/user/j/jecpcl/public/test/plots" // Where you want to save plots etc... - optional functionality
                script.env.plotsurl = "https://your-plots.web.cern.ch/" // URL were the plots are plublished - optional functionality
                break
            case ~/.*-repro$/:
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/tlampen/automation:repro"
                script.env.image_setup = "source /home/jecpcl/setup.sh"
                script.env.dbinstance = "jec_test_v1"
                script.env.campaign = "all"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/5qp5it4w1tnm8je6xt8ya3jcze"
                script.env.eospath = "/eos/user/j/jecpcl/public/test"
                script.env.eosplots = "/eos/user/j/jecpcl/public/test/plots"
                script.env.plotsurl = "https://your-plots.web.cern.ch/"
                break
            default: // Default is the dev image
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/tlampen/automation:dev"
                script.env.image_setup = "source /home/jecpcl/setup.sh"
                script.env.dbinstance = "jec_test_v1"
                script.env.campaign = "2024_physics_v2"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/5qp5it4w1tnm8je6xt8ya3jcze"
                script.env.eospath = "/eos/user/j/jecpcl/public/2024_physics_v2"
                script.env.eosplots = "/eos/user/j/jecpcl/public/test/plots"
                script.env.plotsurl = "https://your-plots-dev.web.cern.ch/"
                break
        }
    }
}
