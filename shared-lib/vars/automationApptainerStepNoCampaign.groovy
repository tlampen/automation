#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: 'jecpcl', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        s_script = "apptainer exec -B /run -B /cvmfs -B /eos -B /afs $image /bin/bash -c 'export KRB5CCNAME=$TMPDIR/krb5cc; export X509_USER_PROXY=/afs/cern.ch/user/j/jecpcl/grid_proxy.x509; echo '$PASSWORD' | kinit $USERNAME; $setup; $script'"
        sh(script: s_script)
    }
}

