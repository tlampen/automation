#!/bin/bash

CMSSW_REL=$1

# exit when any command fails; be verbose
set -ex

# eos-client ("eos ls" and other stuff, not the actual deamon to mount the filesystem, that's done with the -B option by apptainer)
cat << EOF >>/etc/yum.repos.d/eos7-stable.repo
[eos8-stable]
name=EOS binaries from CERN Linuxsoft [stable]
gpgcheck=0
enabled=1
baseurl=http://linuxsoft.cern.ch/internal/repos/eos8-stable/x86_64/os
priority=9
EOF
dnf install --disablerepo epel-next -y --nogpgcheck eos-client

# openssl
dnf install --disablerepo epel-next -y --nogpgcheck openssl-libs openssl-devel

# enable accessing T0 queues
dnf install --disablerepo epel-next -y environment-modules
mkdir -p /etc/modulefiles/lxbatch/
cat << EOF >> /etc/modulefiles/lxbatch/tzero
#%Module 1.0
# module file for lxbatch/share
#
setenv _myschedd_POOL tzero
setenv _condor_CONDOR_HOST "norwegianblue02.cern.ch, tzcm1.cern.ch"
EOF

dnf install --disablerepo epel-next -y bc git

# make cmsrel etc. work
shopt -s expand_aliases
source /cvmfs/cms.cern.ch/cmsset_default.sh

git config --global user.name '${SERVICE_USERNAME}'
git config --global user.email '${SERVICE_USERNAME}@cern.ch'
git config --global user.github '${SERVICE_USERNAME}'

mkdir -p /home/${SERVICE_USERNAME}/
cd /home/${SERVICE_USERNAME}/
cmsrel $CMSSW_REL 
cd $CMSSW_REL/src
cmsenv
git cms-init
cd -

# retrieve JEC analysis code
# mkdir -p /home/$AUTOMATION_USER/JEC
# cd /home/$AUTOMATION_USER/JEC

# Add here cloning of the code
# git clone https://github.com/miquork/dijet
# cd dijet
cd $CMSSW_REL/src
git clone https://github.com/toicca/dijet_automation
git clone https://github.com/toicca/dijet_rdf

# Comment since creates error at the moment
# root -l -q -b mk_CondFormats.C
cd /home/$AUTOMATION_USER/

# manually install the required packages needed for ecalautoctrl 
export PYTHON3PATH=$PYTHON3PATH:/home/${SERVICE_USERNAME}/lib/python3.9/site-packages/
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cmsoms/oms-api-client.git
cd oms-api-client/
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I .
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I influxdb
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I "dbs3-client>=4.0.19"

# install ecalautoctrl
python3 -m pip install git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/ --prefix /home/${SERVICE_USERNAME}/ --no-deps -I

# install pyspark
# python3 -m pip install pyspark==3.1.2 --prefix /home/${SERVICE_USERNAME}/ -I

# install cmsstyle
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I cmsstyle

# install brilws
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I brilws

# copy lib and bin to the CMSSW area
mv /home/${SERVICE_USERNAME}/bin/* /home/${SERVICE_USERNAME}/$CMSSW_REL/bin/$SCRAM_ARCH/
mv /home/${SERVICE_USERNAME}/lib/python3.9/site-packages/* /home/${SERVICE_USERNAME}/$CMSSW_REL/python/

# compile
cd $CMSSW_BASE/src
cmsenv
scram b -j

# set w/r permissions on home directory
chmod 777 /home/${SERVICE_USERNAME}/

# setup script
cat << EOF >> /home/${SERVICE_USERNAME}/setup.sh
#!/bin/bash

source /usr/share/Modules/init/sh
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/${SERVICE_USERNAME}/$CMSSW_REL
cmsenv


echo "The automation T0 API is expecting a path to a VOMS proxy file in the X509_USER_PROXY environment variable which is set to '\$HOME/grid_proxy.x509'."
echo "The file itself can be generated e.g. with 'voms-proxy-init --voms cms --out \\\$HOME/grid_proxy.x509'."
export X509_USER_PROXY=\$HOME/grid_proxy.x509

cd -
# source /cvmfs/sft.cern.ch/lcg/views/LCG_104a/x86_64-centos8-gcc11-opt/setup.sh
# source /cvmfs/sft.cern.ch/lcg/views/LCG_105a/x86_64-el9-gcc13-opt/setup.sh
EOF
